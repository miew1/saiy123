// Define pin numbers for the RGB LED
const int redPin = 9;
const int greenPin = 10;
const int bluePin = 11;

void setup() {
  // Initialize the serial communication
  Serial.begin(9600);
  
  // Set the RGB LED pins as outputs
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  // Turn off all colors initially
  turnOff();
}

void loop() {
  // Check if data is available to read from the serial input
  if (Serial.available() > 0) {
    char input = Serial.read();
    
    switch (input) {
      case 'r':
        setColor(255, 0, 0); // Red
        break;
      case 'g':
        setColor(0, 255, 0); // Green
        break;
      case 'b':
        setColor(0, 0, 255); // Blue
        break;
      case 'O':
        setColor(255, 255, 255); // White
        break;
      case 'o':
        turnOff(); // Off
        break;
      default:
        // If the input is not recognized, do nothing
        break;
    }
  }
}

// Function to set the color of the RGB LED
void setColor(int red, int green, int blue) {
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}

// Function to turn off the RGB LED
void turnOff() {
  analogWrite(redPin, 0);
  analogWrite(greenPin, 0);
  analogWrite(bluePin, 0);
}